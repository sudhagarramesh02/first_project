import os
import sys
import time
import string
import requests
from datetime import datetime
from subprocess import call
from selenium import webdriver
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import csv
from csv import reader
from csv import DictReader


def mysmoke_test(url,name):
    options = webdriver.ChromeOptions()
    options.binary_location = "C:\Program Files\Google\Chrome\Application\chrome.exe"
    options.add_argument('---headless')
    options.add_argument('---no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(executable_path=r'C:\Users\Sudhagar\Desktop\py_code\chromedriver.exe',chrome_options=options)
    #actions = webdriver.ActionChains(driver)
    print("  Status of smoke test is " + url)

    driver.get(url)#put here the adress of your page
    r2 =requests.get(url, verify = False)
    status2 = r2.status_code
    if status2 == 200:
            time.sleep(5)
            #now = datetime.now()
            result2=name
            driver.close()
            print(result2)
    
  
if __name__ == '__main__':
    with open(r'C:\Windows\system32\config\systemprofile\AppData\Local\Jenkins\.jenkins\workspace\CI_exercise\url_lists.csv', 'r') as read_obj:
        # pass the file object to DictReader() to get the DictReader object
        csv_dict_reader = DictReader(read_obj)
        # iterate over each line as a ordered dictionary
        #data = list(csv_dict_reader)
        for row in csv_dict_reader:
                urls = row["url"]
                names = row["name"] 
                mysmoke_test(urls,names)
